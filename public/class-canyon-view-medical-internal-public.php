<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/public
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Internal_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Internal_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Internal_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-internal-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Internal_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Internal_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-internal-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Display the [cvmi_list_provider_languages] shortcode
	 *
	 * @since  1.0.2
	 */	


	
	/**
	 * Display the [cvmi_list_provider_languages] shortcode
	 *
	 * @since  1.0.0
	 */
	public function shortcode_cvmi_list_provider_languages( $atts ) {
		  $a = shortcode_atts( array(
        'table' => false
    ), $atts );


		$sections = get_option( 'cvmi_provider_languages_options', false );
		is_array( $sections ) ? : $sections = [];

		$count = 0;

		$content = '<div id="cmvi_provider_languages_list">';

		if ( $a['table'] ) {
			$content .= '<table id="cmvi_provider_languages_table">
										<thead>
											<tr>
												<th>Name</th>
												<th>Location / Department</th> 
												<th>Languages</th>
											</tr>
										</thead>';

			if ( !empty ($sections) ) {

				$content .= '<tbody>';

				$provider_list = [];

				foreach ( $sections as $section) {
					$heading = $section[heading];
					$providers = $section[providers];

					foreach ( $providers as $provider ) {
						$provider_list[$count] = array('name' => $provider[name],
																						'section' => $heading,
																						'languages' => $provider[languages]
																						);
						$count++;
					}

				}

				foreach ( $provider_list as $provider_details) {
					$content .= '<tr>
												<td>' . $provider_details[name] . '</td>
												<td>' . $provider_details[section] . '</td>
												<td>' . $provider_details[languages] . '</td>
											</tr>';
					
				}


			} else {
				$content .= '<tr>
											<td colspan="3">Sorry, there are no providers listed.</td>
										</tr>';
			}

			$content .= '</tbody>
								</table>';

		} else {
			## Not a table

			$content .= '<div class="gdlr-item gdlr-accordion-item style-2">';

			if ( !empty ( $sections ) ) { 
						
				foreach ( $sections as $section )
				{ 
					$heading = $section[heading];
					$providers = $section[providers];

					if ( $count == 0 ) 
					{
						$isActive = ' active pre-active';
						$count = 1;
					} else {
						$isActive = '';
					}

						$content .= '
							<div class="accordion-tab' . $isActive . '">
								<h4 class="accordion-title"><i class="icon-minus"></i><span>' . $heading . '</span></h4>
								<div class="accordion-content">
									<ul class="provider">';
				
					foreach ( $providers as $provider )
					{
						$content .= '
									<li>' . $provider[name] . ' <span class="language">' . $provider[languages] . '</span></li>';
					}

					$content .= '
								</ul>
							</div>
						</div>';
				}
			}
		} 

		$content .= '
				</div>
			</div>';

		return $content;
	}
	 
	

}
