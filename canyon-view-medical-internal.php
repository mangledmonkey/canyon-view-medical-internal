<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://mangledmonkeymedia.com
 * @since             1.0.0
 * @package           Canyon_View_Medical_Internal
 *
 * @wordpress-plugin
 * Plugin Name:       Canyon View Medical Internal
 * Plugin URI:        https://mangledmonkeymedia.com
 * Description:       A collection of custom tools for the Canyon View Medical internal website. 
 * Version:           1.0.2
 * Author:            Brandon West
 * Author URI:        https://mangledmonkeymedia.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       canyon-view-medical-internal
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-canyon-view-medical-internal-activator.php
 */
function activate_canyon_view_medical_internal() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical-internal-activator.php';
	Canyon_View_Medical_Internal_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-canyon-view-medical-internal-deactivator.php
 */
function deactivate_canyon_view_medical_internal() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical-internal-deactivator.php';
	Canyon_View_Medical_Internal_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_canyon_view_medical_internal' );
register_deactivation_hook( __FILE__, 'deactivate_canyon_view_medical_internal' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-canyon-view-medical-internal.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_canyon_view_medical_internal() {

	$plugin = new Canyon_View_Medical_Internal();
	$plugin->run();

}
run_canyon_view_medical_internal();
