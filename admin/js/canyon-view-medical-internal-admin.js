jQuery(document).ready(function($) {
  'use strict';

  $('#cvmi_provider_languages_settings').on('click', '.cvmi-new-provider', function () {
    var sectionId = $(this).parent().attr('id').replace(/.*-/g, '');
    var providerId = $(this).parent().find('ul.rootlist').children().length;
    var appendProvider = '<li><ul id="cvmi-provider-' + providerId + '" class="cvmi-provider"><li><label for="cvmi-provider-languages-sections" class="cvmi-row-title">Provider: </label><input type="text" name="cvmi_provider_languages_options[' + sectionId + '][providers][' + providerId + '][name]" value="" size="25" /></li><li><label for="cvmi-provider-languages-sections" class="cvm-row-title">Languages: </label><input type="text" name="cvmi_provider_languages_options[' + sectionId + '][providers][' + providerId + '][languages]" value="" size="25"><button class="button cvmi cvmi-remove-provider" type="button">remove</button></li></ul></li>';
    $(this).parent().find('ul.rootlist').append(appendProvider);
  });

  $('#cvmi_provider_languages_settings').on('click', '.cvmi-remove-provider', function () {
    var count = 0;
    var items = $(this).closest('ul.rootlist');
    $(this).closest('ul').parent('li').remove();
    items.find("ul[class*='cvmi-provider-']").each(function () {
      $(this).html(function (index, text) {
        return text.replace(/\[providers\]\[[0-9]{1,2}\]/g, '[providers][' + count + ']');
      });
      count++;
    });
  });

  $('#cvmi_provider_languages_settings').on('click', 'button.cvmi-new-section', function () {
    var lastSection = $(this).parent().find('div[id*="cvmi-provider-languages-section-"]').last().attr('id').replace(/.*-/g, '');
    var sectionId = parseInt(lastSection) + 1;
    var headingId = sectionId + 1;
    var appendSection = '<div id="cvmi-provider-languages-section-' + sectionId + '" class="cvmi-section"><div class="cvmi-section-header"><input type="text" id="hidden-section-heading-' + sectionId + '" name="cvmi_provider_languages_options[' + sectionId + '][heading]" value="Section ' + headingId + '" class="cvmi-section-heading" /></div><ul class="rootlist cvmi-section-content"><li><ul id="cvmi-provider-0" class="cvmi-provider"><li><label for="cvmi-provider-languages-sections" class="cvmi-row-title">Provider: </label><input type="text" name="cvmi_provider_languages_options[' + sectionId + '][providers][0][name]" value="" size="25"></li><li><label for="cvmi-provider-languages-sections" class="cvmi-row-title">Languages: </label><input type="text" name="cvmi_provider_languages_options[' + sectionId + '][providers][0][languages]" value="" size="25"><button class="button cvmi cvmi-remove-provider" type="button">remove</button></li></ul></li></ul><button class="button cvmi cvmi-new-provider" type="button">+ New Provider</button><button class="button cvmi cvmi-remove-section" type="button">Remove Section</button></div>';
    $('div#cvmi_provider_languages_sections').append(appendSection);
  });

  $('#cvmi_provider_languages_settings').on('click', '.cvmi-remove-section', function () {
    if (confirm('Are you sure you want to remove this section?\nChanges will not be permanent until the settings are saved.')) {
      var count = 0;
      var sections = $(this).closest('div#cvmi_provider_languages_sections');
      $(this).closest("div[id*='cvmi-provider-languages-section-']").remove();
      sections.find("div[id*='cvmi-provider-languages-section-']").each(function () {

        $(this).attr('id', function (i, id) {
          if (/cvmi-provider-languages-section-[0-9]{1,2}/.test(id)) {
            return id.replace(/cvmi-provider-languages-section-[0-9]{1,2}/g, 'cvmi-provider-languages-section-' + count);
          }
        });

        $(this).find("input[name*='meta_sections']").attr('name', function (i, name) {
          if (/sections\[[0-9]{1,2}\]/.test(name)) {
            return name.replace(/sections\[[0-9]{1,2}\]/g, 'sections[' + count + ']');
          }
        });

        count++;
      });
    };

  });

});
