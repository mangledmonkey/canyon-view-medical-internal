<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/admin/partials
 */
 
 // check user capabilities
if (!current_user_can('manage_options')) {
    return;
}

// add error/update messages

// check if the user have submitted the settings
// wordpress will add the "settings-updated" $_GET parameter to the url
if (isset($_GET['settings-updated'])) {
    // add settings saved message with the class of "updated"
    add_settings_error('wporg_messages', 'wporg_message', __('Settings Saved', 'wporg'), 'updated');
}

// show error/update messages
settings_errors('wporg_messages');
?>
<div class="wrap">
  <div id="cvmi_provider_languages_settings">

    <form method="post" action="options.php">
      <?php settings_fields( 'cvmi_provider_languages' ); ?>
      <?php do_settings_sections( 'cvmi_provider_languages' ); ?>

      <?php submit_button(); ?>

    </form>
  </div>
</div>