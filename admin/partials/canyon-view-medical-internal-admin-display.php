<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
