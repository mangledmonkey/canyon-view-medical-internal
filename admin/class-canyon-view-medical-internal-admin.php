<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/admin
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Internal_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Internal_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Internal_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/canyon-view-medical-internal-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Canyon_View_Medical_Internal_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Canyon_View_Medical_Internal_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/canyon-view-medical-internal-admin.js', array( 'jquery' ), $this->version, false );

	}
	
		/**
	 * Add an admin page to menu
	 *
	 * @since  1.0.0
	 */
	public function add_admin_page() {
		add_menu_page(
			'Canyon View Medical Info Settings', //$page_title
			'CVM Info', //$menu_title
			'manage_options', //$capability
			'canyon-view-medical-internal-provider-languages', //$menu_slug
			array($this, 'display_admin_page'), //$function
			'', //$icon_url
			'3.0' //$position number on menu from top
		);
		
		add_submenu_page(
			'canyon-view-medical-internal-admin', //$parent_slug
			'Provider Languages', //$page_title
			'Provider Languages', //$menu_title
			'manage_options', //$capability,
			'canyon-view-medical-internal-provider-languages', //$menu_slug
			array($this, 'display_provider_languages_page') //$function	
		);
		
	}

  /**
   * Define setttings for admin page options
   *
   * @since  1.0.0
 	 */
	public function cvmi_provider_languages_settings_init() {

		// register new setting for Provider Languages page
		register_setting( 'cvmi_provider_languages', 'cvmi_provider_languages_options');

		// register new section in the Provider Languages page
		add_settings_section( 
			'cvmi_section_provider_languages',
			__('Provider Languages', 'cvmi_provider_langugaes'),
			array($this, 'cvmi_provider_languages_callback'),
			'cvmi_provider_languages'
		);
		
		add_settings_field(
			'cvmi_provider_languages_sections',
			__('Providers', 'cvmi_provider_languages'),
			array($this, 'cvmi_provider_language_settings_callback'),
			'cvmi_provider_languages',
			'cvmi_section_provider_languages' 
		);
		
	}

	/**
	 * Display the admin page
	 *
	 * @since  1.0.0
	 */
	public function display_admin_page() {
		include_once 'partials/canyon-view-medical-internal-admin-display.php';
	}
	
	/**
	* Prints the provider in a section
	*
	* @since		1.0.0
	*/
	function print_cvmi_section_providers($provider, $cnt, $p) {
	?> 
		<li>
			<ul id="cvmi-provider-<?php echo $p; ?>" class="cvmi-provider">
				<li>
					<label for="cvmi-provider-languages-sections" class="cvmi-row-title">Provider: </label>
					<input type="text" name="cvmi_provider_languages_options[<?php echo $cnt; ?>]['providers'][<?php echo $p; ?>]['name']"
									value="<?php echo esc_attr( $provider['name'] ); ?>" size="25">
				</li>
				<li>
					<label for="cvmi-provider-languages-sections" class="cvmi-row-title">Languages: </label>
					<input type="text" name="cvmi_provider_languages_options[<?php echo $cnt; ?>]['providers'][<?php echo $p; ?>]['languages']"
									value="<?php echo esc_attr( $provider['languages'] ); ?>" size="25">
					<button class="button cvmi cvmi-remove-provider"
									type="button">remove</button>
				</li>
			</ul>
		</li>
		<?php
	}

	/**
	* Add a section to the provder languages
	*
	* @since		1.0.0
	*/
	function print_cmvi_providers_section($section, $cnt) {
		?>
		<div id="cvmi-provider-languages-section-<?php echo $cnt; ?>" class="cvmi-section">
			<div class="cvmi-section-header">
				<input type="text" id="hidden-section-heading-<?php echo $cnt; ?>"
							name="cvmi_provider_languages_options[<?php echo $cnt; ?>]['heading']"
							value="<?php echo esc_attr( $section['heading'] ); ?>"
							class="cvmi-section-heading"
							size="50"> 
			</div>
			<ul class="rootlist cvmi-section-content">
				<?php for ($p = 0; $p < count($section['providers']); $p++ ) {
					$provider = $section['providers'][$p];
					$this->print_cvmi_section_providers($provider, $cnt, $p);
				}
				?>
			</ul>

			<button class="button cvmi cvmi-new-provider"
							type="button">+ New Provider</button>
			<button class="button cvmi cvmi-remove-section"
							type="button">Remove Section</button>
		</div>
		<?php
	}
		
	/**
	* Add a settings section 
	*
	* @since		1.0.0
 	*/
	function cvmi_provider_language_settings_callback($args) {
		$sections = get_option( 'cvmi_provider_languages_options', false );
		is_array( $sections ) ? : $sections = [];
		?>
        
		<div id="cvmi_provider_languages_sections">
			<?php if ( empty ( $sections ) ) { ?>
				<?php
				$emptySection = array(
													'heading' => 'Section Title',
													'providers' => array(
														0 => array (
															'name' => 'Provider Name',
															'languages' => 'Provider Languages'
														)
													)
												);
				$this->print_cmvi_providers_section($emptySection, 0);
			?> 
			<?php 
				} else {
			?>
			<?php
				for ($i = 0; $i < count($sections); $i++) {
					$this->print_cmvi_providers_section($sections[$i], $i);
				} 
			}
			?>
			</div>
			<button id="cvmi-provider-languages-new-section-button"
							class="button cvmi cvmi-new-section"
							type="button">+ New Section</button>

	<?php
	}

	function cvmi_provider_languages_callback($args) {
		?>
		<p id="<?= esc_attr($args['id']); ?></h1>"><?= esc_html__('List providers and languages by location', 'cmvi_provider_languages'); ?></p>
		<p>Use the shortcode `<span class="code">[cvmi_list_provider_languages]</span>` to display the list in a page or post.</p>
		<p>Use the `<span class="code">table</span>` attribute, `<span class="code">[cvmi_list_provider_languages table=true]</span>` to display the list as a table.</p>
		<?php
	}


	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	function display_provider_languages_page() {
		include_once 'partials/canyon-view-medical-internal-provider-languages-display.php';
	}
	


}
