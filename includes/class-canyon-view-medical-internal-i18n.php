<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://mangledmonkeymedia.com
 * @since      1.0.0
 *
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Canyon_View_Medical_Internal
 * @subpackage Canyon_View_Medical_Internal/includes
 * @author     Brandon West <brandon@mangledmonkeymedia.com>
 */
class Canyon_View_Medical_Internal_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'canyon-view-medical-internal',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
